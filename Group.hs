module Group where

data Group = Group { id :: Int
                   , size :: Int
		   , courses :: [ Int ] } deriving (Show, Eq)

newGroup :: Int -> Int -> [ Int ] -> Group
newGroup i s cs = Group { Group.id = i
                        , size = s
		        , courses = cs }

addCourse :: Group -> Int -> Group
addCourse g c = g { courses = c : courses g }
