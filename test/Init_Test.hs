module Init_Test where

import Room
import Professor
import Course
import Group
import TimeSlot
import Class
import Init
import School
import Test.HUnit

test_nbrClasses1 = 
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3 ] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let n = nbrClasses s in
  TestCase $ assertEqual "Test number of classes equal to 1"
			 1 n 

test_nbrClasses2 = 
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8 ] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let n = nbrClasses s in
  TestCase $ assertEqual "Test number of classes equal to 2"
			 2 n 

main = runTestTT $ TestList [ test_nbrClasses1              -- 0
			    , test_nbrClasses2 ]            -- 1 
