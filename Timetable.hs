module Timetable where

import Class
import Group
import Room
import School
import Individual

type Timetable = [ Class ]

-- Reads the chromosome and creates a timetable from it
createClasses :: Individual -> School -> Timetable
createClasses c s = 
  let gs = groups s in
  createClasses' 0 gs where
    createClasses' i gs' = case gs' of
      [] -> []
      g:gs' ->
        let cs = genes c in
        let xs = Group.courses g in
        let gid = Group.id g in
        [ newClass (i+n) gid x p t r | (n, x) <- zip [ 0 .. ] xs
	                             , (m1, p) <- cs, m1 == (i+n)*3
				     , (m2, t) <- cs, m2 == m1+1
				     , (m3, r) <- cs, m3 == m1+2 ] 
	++ createClasses' (i + length xs) gs'

-- check if group size fits in room
checkRoom :: Int -> Int -> Int
checkRoom capacity groupSize = if capacity - groupSize > 0 then 0 else 1

-- Checkif room is taken
checkRoomTimeSlot :: Class -> [ Class ] -> Int
checkRoomTimeSlot _ [] = 0
checkRoomTimeSlot c (c':cs') =
  if timeSlot c - timeSlot c' /= 0 then 0 else 1 + checkRoomTimeSlot c cs'

-- Check if professor is available
checkProfessorTimeSlot :: Class -> [ Class ] -> Int
checkProfessorTimeSlot _ [] = 0
checkProfessorTimeSlot c (c':cs') = 
  if timeSlot c - timeSlot c' /= 0 then 0 else 1 + checkProfessorTimeSlot c cs'

-- Calculate the number of clashes in timetable
-- The calcClashes method checks each one in
-- turn and counts the number of clashes. In this case, a clash is any hard constraint
-- violation, such as a class whose room is too small, a conflict with room and timeslot,
-- or a conflict with professor and timeslot. The number of clashes is used later by the
-- GeneticAlgorithm's calcFitness method
calcClashes :: Timetable -> School -> Int
calcClashes [] _ = 0
calcClashes (c:cs) s =
  let cs' = filter (\ c' -> room c == room c') cs in
  let cs'' = filter (\ c'' -> professor c == professor c'') cs in
    checkRoom (capacity (findRoom s (room c))) (size (findGroup s (group c)))
    + checkRoomTimeSlot c cs'
    + checkProfessorTimeSlot c cs''
    + calcClashes cs s
