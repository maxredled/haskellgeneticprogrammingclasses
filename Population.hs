module Population where

import Data.Maybe
import System.Random
import Individual
import School
import Timetable
import Data.List
import Lib

-- A chromosome is a sequense of prof, time slot, room 
type Population = [ Individual ]

calcFitness :: School -> Population -> Population 
calcFitness _ [] = []
calcFitness s (c:cs) = c { fitness = Just (calcClashes (createClasses c s) s) } 
                     : calcFitness s cs

calcFitnessPopulation :: Population -> Int
calcFitnessPopulation p = 
  sum $ map fit p
    where
      fit x = fromMaybe (error "Clashes unknown, module Population, function calcFitnessPopulation") 
                        (fitness x)

ordPopulation :: Population -> Population
ordPopulation = sort

tournamentSelection :: Int -> Population -> IO Individual 
tournamentSelection n p = do -- tournament size, previous population
  -- Tournament selection selects its parents by running a series of "tournaments".
  -- First, individuals are randomly selected from the population and entered into a
  -- tournament. Next, these individuals can be thought to compete with each other
  -- by comparing their fitness values, then choosing the individual with the highest
  -- fitness for the parent.
  is <- randList n p
  let individual = head $ ordPopulation is
  return individual

selectParents :: Int -> Population -> IO (Individual, Individual)
selectParents n p = do -- tournament size, previous population
  parent1 <- tournamentSelection n p
  parent2 <- tournamentSelection n p
  return (parent1, parent2)

crossover :: Float -> (Individual, Individual) -> School -> IO Individual 
crossover c parents s = do -- crossover rate, (first parent, second parent), 
                           -- school
  -- Single point crossover is a very simple crossover method in which a single 
  -- position in the genome is chosen at random to define which genes come from which
  -- which parent. The genetic information before the crossover position comes from
  -- parent1, while the genetic information, after the position, comes from parent2.
  gen <- newStdGen
  let r = head $ take 1 $ randoms gen :: Float 
  if c < r 
    then return (fst parents)
    else do
      c <- createChromosome s 
      let auxIndividual = newIndividual (zip [ 0 .. ] c) Nothing

      -- random swap point
      gen <- newStdGen
      let (pos, _) = randomR (0, length (genes (fst parents)) - 1) gen
      let mixGene i = if i < pos 
          then (i, snd $ genes (fst parents) !! i)
          else (i, snd $ genes (snd parents) !! i)
      let chr = map (mixGene . fst ) $ genes auxIndividual

      let individual = (newIndividual chr Nothing)
      return individual


offspring :: Int -> Int -> Float -> Float -> School -> Population
          -> IO Population
offspring 0 _ _ _ _ _ = return [] 
offspring n tSize m c s p = do -- elite, tournament size, mutation rate,
                               -- crossover rate, school, 
			       -- previous population
  parents <- selectParents tSize p
  chr <- crossover c parents s
  individual <- mutation m s chr

  rest <- offspring (n-1) tSize m c s p
  return (individual : rest)
  
mutation :: Float -> School -> Individual -> IO Individual 
mutation m s i = do -- mutation rate, school, individual
  -- In uniform crossover, genes are selected at random from an existing and valid parent.
  -- The parent might not be the fittest individual in the population, but at least it's
  -- valid.
  -- Here we create a new random but valid individual and essentially run uniform crossover
  -- to achieve mutation! Afterward we select genes from the random Individual to copy into 
  -- the Individual to be mutated. This technique is called uniform mutation, and makes
  -- sure that all of our mutated individuals are fully valid, never selecting a gene that 
  -- doesn't make sense.

  -- Create random individual to swap genes with
  c <- createChromosome s
  let randomIndividual = newIndividual (zip [ 0 .. ] c) Nothing

  -- Swap genes according to mutation rate
  let swapGene g = do
        gen <- newStdGen
        let r = head $ drop (snd g) $ randoms gen :: Float
        if m < r
          then return g
	  else do
	    return (fst g, snd $ genes randomIndividual !! fst g)
  c' <- sequence (map swapGene $ genes i)
  return (newIndividual c' $ Nothing)

newGeneration :: Int -> Int -> Float -> Float -> School -> Population
              -> IO Population
newGeneration e tSize m c s p = do -- elite, tournament size, mutation rate,
                                   -- crossover rate, school, 
				   -- previous population
  let pElite = take e $ ordPopulation p
  p' <- offspring (length p - e) tSize m c s p
  return (calcFitness s $ pElite ++ p')
