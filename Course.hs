module Course where

data Course = Course { id :: Int
                     , code :: String 
		     , name :: String
		     , professors :: [ Int ] } deriving (Show, Eq)

newCourse :: Int -> String -> String -> [ Int ] -> Course

newCourse i c n ps = Course { Course.id = i 
                            , code = c
			    , name = n
			    , professors = ps }
