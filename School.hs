module School where

import Room
import Professor
import Course
import Group
import TimeSlot
import Class

data School = School { rooms :: [ Room ] 
                     , professors :: [ Professor ]
                     , courses :: [ Course ]
                     , groups :: [ Group ]
                     , timeSlots :: [ TimeSlot ] } deriving (Show, Eq)

newSchool :: [ Room ] -> [ Professor ] -> [ Course ] -> [ Group ] -> [ TimeSlot ] -> School
newSchool rs ps cs gs ts = 
  School { rooms = rs
         , School.professors = ps
         , School.courses = cs
         , groups = gs
         , timeSlots = ts }

findCourse :: School -> Int -> Course
findCourse s id = head $ filter (\ c -> Course.id c == id) $ School.courses s

findGroup :: School -> Int -> Group
findGroup s id = head $ filter (\ g -> Group.id g == id) $ groups s

findProfessor :: School -> Int -> Professor
findProfessor s id = head $ filter (\ p -> Professor.id p == id) $ School.professors s

findRoom :: School -> Int -> Room
findRoom s id = head $ filter (\ r -> Room.id r == id) $ rooms s

findTimeSlot :: School -> Int -> TimeSlot
findTimeSlot s id = head $ filter (\ t -> TimeSlot.id t == id) $ timeSlots s

nbrClasses :: School -> Int
nbrClasses s =
  let gs = groups s in
  sum $ map (length . Group.courses) gs
